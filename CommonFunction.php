<?php
//store in the common functions git here
//https://gitlab.com/locddspkt/CommonFunction.git
//todo: move the project base feature to [ProjectCode]Helpers
use app\models\Customer;
use app\models\User;
use app\models\EmailMessage;
use app\models\CustomerParent;
use app\helpers\CookieHelper;
use app\helpers\Session;
use App\Helpers\S3Helper;
use \App\Helpers\TurncoHelpers;

if (!defined('abcdefghijklmnopqrstuvwxyz')) define('abcdefghijklmnopqrstuvwxyz', 'abcdefghijklmnopqrstuvwxyz');
if (!defined('ABCDEFGHIJKLMNOPQRSTUVWXYZ')) define('ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
if (!defined('_0123456789')) define('_0123456789', '0123456789');
if (!defined('SPECIAL_CHARACTERS')) define('SPECIAL_CHARACTERS', '~`!@#$%^&*(){}[]\|:;"\'<>?/');


$workingDays = array(1, 2, 3, 4, 5); # date format = N
$holidayDays = array('*-12-25', '*-01-01', '2013-12-23'); # variable and fixed holidays

$day_work = 32400; // 9 * 60 * 60
$hour_start_day = '08:00:00';
$hour_end_day = '17:00:00';

class CommonFunction {

    //save cache for 'php://input'
    private static $jsonPostedData = null;

    public static function getJsonPostedValue($name) {
        if (self::$jsonPostedData == null) self::$jsonPostedData = json_decode(file_get_contents('php://input'));
        $postedData = self::$jsonPostedData;

        if (isset($postedData) && isset($postedData->$name)) return $postedData->$name;

        //try with Post
        if (isset($_POST[$name])) return $_POST[$name];

        return '';
    }

    /***
     * get the detail in the posted name data
     * @param $name
     * @return string
     */
    public static function getJsonPostedDataByName($name) {
        $data = CommonFunction::getJsonPostedValue('data');

        if (empty($data)) return '';

        if (isset($data->$name)) return $data->$name;

        if (!is_array($data)) return '';

        //try array
        if (isset($data[$name])) return $data[$name];
        return '';
    }

    public static function getPostValue($name, $index = false) {
        if ($index === false) {
            if (isset($_POST[$name])) return $_POST[$name];
            return "";
        }
        else {
            if (isset($_POST[$name]) && isset($_POST[$name][$index])) return $_POST[$name][$index];
            return "";
        }

    }

    public static function getJsonOrRequestPosted($name, $index = false) {
        //try json first
        if (self::$jsonPostedData == null) self::$jsonPostedData = json_decode(file_get_contents('php://input'));
        $postedData = self::$jsonPostedData;

        if (isset($postedData) && isset($postedData->$name)) return $postedData->$name;

        //then try post
        return static::getPostValue($name, $index);
    }

    public static function setPostValue($name, $value, $index = false) {
        if ($index === false) {
            $_POST[$name] = $value;
        }
        else {
            if (!isset($_POST[$name])) $_POST[$name] = [];
            $_POST[$name][$index] = $value;
        }
    }

    public static function getGetValue($name) {
        if (isset($_GET[$name])) return $_GET[$name];
        return "";
    }

    public static function setGetValue($name, $value) {
        $_GET[$name] = $value;
    }

    public static function GetSessionValue($name) {
        if (isset($_SESSION[$name])) return $_SESSION[$name];
        return NULL;
    }

    public static function CheckPostSend() {
        return count($_POST) != 0;
    }

    public static function CheckGetSend() {
        return count($_GET) != 0;
    }

    public static function guid($hasHyphens = true, $hasBraces = false) {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        }
        else {
            mt_srand((double)microtime() * 10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = $hasHyphens ? chr(45) : '';// "-"
            $braceStart = $hasBraces ? chr(123) : '';
            $braceEnd = $hasBraces ? chr(125) : '';
            if ($hasHyphens == false) {
                $hyphen = '';
            }
            $uuid = $braceStart// "{"
                . substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12)
                . $braceEnd;// "}"
            return $uuid;
        }
    }

    public static function safeDefine($key, $value) {
        if (!defined($key)) {
            define($key, $value);
        }
    }

    public static function defineSaltKey() {
        if (IS_LOCAL) {
            CommonFunction::safeDefine('SALT', '1234567890123456');
        }
        else {
            CommonFunction::safeDefine('SALT', 'turnco sentry');
        }
    }

    public static function getHttpProtocal() {
        if (IS_LOCAL) return 'http';

        return 'https';
    }

    public static function getRootUrl() {
        $http = CommonFunction::getHttpProtocal();
        return "$http://{$_SERVER["HTTP_HOST"]}";
    }

    public static function getLoggedUserId() {
        //try session first
        $currentLoggedUserId = Session::read('logged_user_id');
        if (!empty($currentLoggedUserId)) return $currentLoggedUserId;

        //try the cookie
        if (!isset($_COOKIE['turnco'])) $_COOKIE['turnco'] = '';

        if (empty($_COOKIE)) return 0;
        if (empty($_COOKIE['turnco'])) return 0;

        $authdata = CommonFunction::getAuthDataFromCookie();

        $authparts = explode("|", $authdata);

        if (empty($authparts[0])) return 0;
        //check existed user in this cookie
        $userId = $authparts[0];

        return $userId;
    }

    //change:  do not use cookie anymore, use session instead
    //change: in the time between 2 system, still try to use the cookie
    public static function checkCookieAndRedirectRoot() {
        $rootUrl = CommonFunction::getRootUrl();

        $currentLoggedUserId = static::getLoggedUserId();

        if (empty($currentLoggedUserId)) {
            header("Location: $rootUrl");
            exit;
        }
    }

    public static function auth_decrypt($text) {
        CommonFunction::defineSaltKey();
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }

    public static function auth_encrypt($text) {
        CommonFunction::defineSaltKey();
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

    //alias
    public static function AuthEncrypt($text) {
        return self::auth_encrypt($text);
    }

    public static function AuthDecrypt($text) {
        return self::auth_decrypt($text);
    }

    public static function AuthDecryptOnUrlParam($text) {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, base64_decode(str_replace(" ", "+", $text)), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }

    public static function getAuthDataFromCookie() {
        CommonFunction::defineSaltKey();
        return CommonFunction::auth_decrypt($_COOKIE['turnco']);
    }


    /**
     * @deprecated deprecated, use DbConnection::queryFirstRow instead
     */
    public static function getFirstRowDb($sql) {
        return DbConnection::queryFirstRow($sql);
    }

    /***
     * get the location for customer of Turnco (apply for customer only ulevel = 4,5,6
     * @param $userId
     * @return array list of the locations of the specific user
     */
    public static function getLocationsForCustomerByUserId($userId) {
        $user = User::getUserById($userId);
        $customerId = $user->customerid;

        $ulevel = $user->ulevel;
        $customer = Customer::getById($customerId);

        $data = new stdClass();
        if ($ulevel == U_LEVEL_LOCATION_ADMIN) {
            $data->locations = [$customer];
            $data->parent = CustomerParent::getById($customer->parentid);
        }
        else if ($ulevel == U_LEVEL_REGIONAL_ADMIN) {
            //temporary resume that one the region is belong to only one parent
            //1. get all the customer with the regionid
            $data->locations = $customers = Customer::getCustomerByRegionAndParent($customer->regionid, $customer->parentid);

            //get all the parent customer with the given locations
            $parents = [];
            foreach ($customers as $checkingCustomer) {
                //check if parentid in the parents or not
                if (CommonFunction::checkParentIdInList($checkingCustomer->parentid, $parents)) continue;

                //not in list yet, add
                $parents[] = CustomerParent::getById($checkingCustomer->parentid);
            }

            $data->parents = $parents; //do not use now, temporary clear it
            $data->parent = CustomerParent::getById($customer->parentid);

            unset($data->parents);
        }
        else if ($ulevel == U_LEVEL_COMPANY_ADMIN) {
            $parentId = $customer->parentid;
            $data->parent = CustomerParent::getById($parentId);
            $data->locations = Customer::getCustomersByParentId($parentId);
        }

        return $data;
    }

    public static function checkParentIdInList($parentId, $parents) {
        foreach ($parents as $parent) {
            if ($parent->id == $parentId) return true;
        }

        return false;
    }

    //change: this is not used anymore because the file header.php always override this
    public static function getNavMenuByUserLevel($ulevel) {
        if (empty($ulevel)) return [];

        if ($ulevel < 7) {
            if ($ulevel == 1) {
                $navLinks = array(
                    '/welcome.php' => 'Inspection',
                    '/trs/refurb.php' => 'Refurb',
                    '/reports.php' => 'Reports',
                    '/download.php' => 'Download',
                    '/utilities.php' => 'Utilities',
                    '/account.php' => 'My Account',
                    '/logout.php' => 'Logout');
            }
            else if ($ulevel == 4 || $ulevel == 5) {
                $navLinks = array(
                    '/welcome.php' => 'Inspection',
                    '/trs/refurb_c.php' => 'Refurb',
                    '/reports.php' => 'Reports',
                    '/account.php' => 'My account', '
                    /logout.php' => 'Logout');
            }
            else {
                $navLinks = array(
                    '/welcome.php' => 'Inspection',
                    '/reports.php' => 'Reports',
                    '/account.php' => 'My account',
                    '/logout.php' => 'Logout');
            }
        }
        else {
            $navLinks = array(
                '/welcome.php' => 'Inspection',
                '/account.php' => 'My account',
                '/logout.php' => 'Logout');
        }

        return $navLinks;
    }

    //use in header.php

    /***
     * @deprecated use TurncoHelpers
     * @return array
     */
    public static function getNavMenuForCurrentUser() {
        return TurncoHelpers::getNavMenuForCurrentUser();
    }

    //every instance S3 client need to be called this function, and the instance is only init once life time
    private static $s3Client = NULL;

    /**
     * @deprecated deprecated, use S3Helper::getS3Client instead
     */
    public static function initS3Client() {
        return self::$s3Client = S3Helper::getS3ClientInstance();
    }

    //ham lay tu tren web
    public static function get_random_string($valid_chars, $length) {
        // start with an empty random string
        $random_string = "";

        // count the number of chars in the valid chars string so we know how many choices we have
        $num_valid_chars = strlen($valid_chars);

        // repeat the steps until we've created a string of the right length
        for ($i = 0; $i < $length; $i++) {
            // pick a random number from 1 up to the number of valid chars
            $random_pick = mt_rand(1, $num_valid_chars);

            // take the random character out of the string of valid chars
            // subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
            $random_char = $valid_chars[$random_pick - 1];

            // add the randomly-chosen char onto the end of our string so far
            $random_string .= $random_char;
        }

        // return our finished random string
        return $random_string;
    }

    public static function getLatestDayOfMonth($year, $month) {
        $date = date_create_from_format('Y-n-j', "$year-$month-1");
        $date->modify('+1 month');
        $date->modify('-1 day');
        return $date->format('j');
    }

    public static function getRandomDate($yearFrom = false, $monthFrom = false, $dayFrom = false, $yearTo = false, $monthTo = false, $dayTo = false) {
//        echo ($yearFrom === false?'false':$yearFrom) . '-' . ($monthFrom === false?'false':$monthFrom) . '-' . ($dayFrom===false?'false':$dayFrom);
//        echo ' ' . ($yearTo === false?'false':$yearTo) . '-' . ($monthTo === false?'false':$monthTo) . '-' . ($dayTo===false?'false':$dayTo);
//        echo "\n";

        $yearRange = 3000;

        /*--- BEGIN convert all to random in the same year ---*/
        //has no year from, year to -> dont care month day
        if ($yearFrom === false && $yearTo === false) {
            $year = rand(1970, max(date('Y') + 1000, 1970 + $yearRange));
            return CommonFunction::getRandomDate($year, 1, 1, $year, 12, 31);
        }

        //has year from, no year to => dont care month day to
        if ($yearFrom !== false && $yearTo === false) {
            $yearTo = rand($yearFrom, $yearFrom + $yearRange);
            return CommonFunction::getRandomDate($yearFrom, $monthFrom, $dayFrom, $yearTo, 12, 31);
        }

        //has year to, no year from => dont care month day from
        if ($yearFrom === false && $yearTo !== false) {
            $yearFrom = rand(1970, $yearTo);
            return CommonFunction::getRandomDate($yearFrom, 1, 1, $yearTo, $monthTo, $dayTo);
        }

        //has year from and year to
        if ($yearFrom !== false && $yearTo !== false && $yearFrom != $yearTo) {
            //when the year is the same, return random of that year
            if ($yearFrom > $yearTo) {
                $swap = $yearFrom;
                $yearFrom = $yearTo;
                $yearTo = $swap;

                $swap = $monthFrom;
                $monthFrom = $monthTo;
                $monthTo = $swap;

                $swap = $dayFrom;
                $dayFrom = $dayTo;
                $dayTo = $swap;
            }

            $randomYear = rand($yearFrom, $yearTo);

            if ($yearFrom < $randomYear && $randomYear < $yearTo) return self::getRandomDate($randomYear, 1, 1, $randomYear, 12, 31);

            if ($randomYear == $yearFrom) return self::getRandomDate($randomYear, $monthFrom, $dayFrom, $randomYear, 12, 31);
            if ($randomYear == $yearTo) return self::getRandomDate($randomYear, 1, 1, $randomYear, $monthTo, $dayTo);
        }

        /*--- END convert all to random in the same year ---*/

        //until hear, year is the same always
        $year = $yearFrom;
        //until here year has value and get the random for the exact year

        //no month => dont care day from to
        if ($monthFrom === false && $monthTo === false) {
            $randomMonth = rand(1, 12);
            return self::getRandomDate($year, $randomMonth, 1, $year, $randomMonth, self::getLatestDayOfMonth($year, $randomMonth));
        }

        //no month to
        if ($monthFrom !== false && $monthTo === false) {
            $monthTo = rand($monthFrom, 12);
            return self::getRandomDate($year, $monthFrom, $dayFrom, $year, $monthTo, self::getLatestDayOfMonth($year, $monthTo));
        }

        //no month from
        if ($monthFrom === false && $monthTo !== false) {
            $monthFrom = rand(1, $monthTo);
            return self::getRandomDate($year, $monthFrom, 1, $year, $monthTo, $dayTo);
        }

        //has month from and to
        if ($monthFrom !== false && $monthTo !== false && $monthTo !== $monthFrom) {
            if ($monthFrom > $monthTo) {
                $swap = $monthFrom;
                $monthFrom = $monthTo;
                $monthTo = $swap;

                $swap = $dayFrom;
                $dayFrom = $dayTo;
                $dayTo = $swap;
            }

            $randomMonth = rand($monthFrom, $monthTo);
            if ($monthFrom < $randomMonth && $randomMonth < $monthTo) return self::getRandomDate($year, $randomMonth,1, $year,$randomMonth,self::getLatestDayOfMonth($year, $randomMonth));

            if ($dayFrom === false) $dayFrom = 1;
            if ($dayTo === false) $dayTo = CommonFunction::getLatestDayOfMonth($year, $monthTo);

            if ($randomMonth === $monthFrom) return self::getRandomDate($year, $randomMonth, $dayFrom, $year, $randomMonth, self::getLatestDayOfMonth($year, $randomMonth));
            if ($randomMonth === $monthTo) return self::getRandomDate($year, $randomMonth, 1, $year, $randomMonth, $dayTo);
        }



        //until here, the same month
        $month = $monthFrom;
        if ($dayFrom === false && $dayTo === false) {
            $day = rand(1,self::getLatestDayOfMonth($year, $month));
            $randomDate = date_create_from_format('Y-n-j', "$year-$month-$day");
            $randomDateFormat = $randomDate->format('Y-m-d');
            return $randomDateFormat;
        }

        //no day from
        if ($dayFrom === false) {
            $day = rand(1, $dayTo);
            $randomDate = date_create_from_format('Y-n-j', "$year-$month-$day");
            $randomDateFormat = $randomDate->format('Y-m-d');
            return $randomDateFormat;
        }

        if ($dayTo === false) {
            $day = rand($dayFrom, self::getLatestDayOfMonth($year,$month));
            $randomDate = date_create_from_format('Y-n-j', "$year-$month-$day");
            $randomDateFormat = $randomDate->format('Y-m-d');
            return $randomDateFormat;
        }

        //has day from and to
        if ($dayFrom == $dayTo) {
            $randomDate = date_create_from_format('Y-n-j', "$year-$month-$dayFrom");
            $randomDateFormat = $randomDate->format('Y-m-d');
            return $randomDateFormat;
        }

        if ($dayFrom > $dayTo) {
            $swap = $dayFrom;
            $dayFrom = $dayTo;
            $dayTo = $swap;
        }

        $day = rand($dayFrom,$dayTo);
        $randomDate = date_create_from_format('Y-n-j', "$year-$month-$day");
        $randomDateFormat = $randomDate->format('Y-m-d');
        return $randomDateFormat;
    }

    public static function cleanUpText($thetext) {
        return addslashes(trim($thetext));
    }

    public static function getCurrentTimeWithMillisecond($microtime = null) {
        if (empty($microtime)) $microtime = microtime();
        $now = $microtime;
        $now = explode(' ', $now);
        $timestamp = $now[1] + 0;
        $milliseconds = (int)(1000 * (float)$now[0]);

        $now = new stdClass();
        $now->year = (int)date('Y', $timestamp);
        $now->month = (int)date('m', $timestamp);
        $now->day = (int)date('d', $timestamp);
        $now->hour = (int)date('H', $timestamp);
        $now->minute = (int)date('i', $timestamp);
        $now->second = (int)date('s', $timestamp);
        $now->millisecond = $milliseconds;

        return $now;
    }

    //change: add session for more security
    public static function setTurncoCookieForLogin($userId) {
        CommonFunction::defineSaltKey();
        $encryptionText = CommonFunction::auth_encrypt($userId . "|" . time());

        // Save session cookie
        //locdd: when local or mydev.tuncollc.com, update the max time for cookie
        if (IS_LOCAL || IS_DEV_PRODUCTION) {
            //ten years is long enough
            CookieHelper::setcookie("turnco", $encryptionText, time() + 60 * 60 * 24 * 365 * 10, '/', $_SERVER["HTTP_HOST"], 0);
        }
        else {
            CookieHelper::setcookie("turnco", $encryptionText, time() + 604800, '/', $_SERVER["HTTP_HOST"], 0);
        }
        Session::write('logged_user_id', $userId);
    }

    public static function setTurncoCookieForLogout() {
        CookieHelper::setcookie("turnco", '', '-860000', '/', $_SERVER['HTTP_HOST'], 0); //set this for other features to use.
        Session::clearSession('logged_user_id');
    }

    //for safe, include email library here
    public static function sendEmailWithSwift(EmailMessage $emailMessage) {
        if (IS_LOCAL) {
            require_once __DIR__ . '/../globals/swift/lib/swift_required.php';
            require_once __DIR__ . '/../globals/amazon_creds.php';
        }
        else {
            require_once '/var/www/globals/swift/lib/swift_required.php';
            require_once '/var/www/globals/amazon_creds.php';
        }

        //check valid email message first
        //do not allow empty subject, body
        if (empty($emailMessage->subject) || empty($emailMessage->body) || empty($emailMessage->toEmails)) return false;

        $transport = Swift_SmtpTransport::newInstance(AMAZON_EMAIL_SMTP_HOST, AMAZON_EMAIL_SMTP_PORT, AMAZON_EMAIL_SMTP_SECURITY)
            ->setUsername(AMAZON_EMAIL_SMTP_USERNAME)
            ->setPassword(AMAZON_EMAIL_SMTP_PASSWORD);

        $mailer = Swift_Mailer::newInstance($transport);
        $message = Swift_Message::newInstance();


        //from
        //change: always use from of Goftec

        /*---
        if (!empty($emailMessage->fromEmails)) {
            $fromEmails = EmailMessage::splitArrayToKeyArray($emailMessage->fromEmails);
            // $fromEmails is now email => display

            if (IS_LOCAL) {
                //send from locddspkt@gmail with display = array [$email=>display|$email=>display
                $fromEmail = 'locddspkt@gmail.com';
                $display = '';
                foreach ($fromEmails as $email=>$display) {
                    $display .= "$email=>$display";
                }
                $message->setFrom([$fromEmail => $display]);
            }
            else {
                $message->setFrom($fromEmails);
            }
            //when has only one
        }
        --*/

        $fromEmails = EmailMessage::splitArrayToKeyArray($emailMessage->fromEmails);
        $message->setFrom($fromEmails);

        //to
        $toEmails = EmailMessage::splitArrayToKeyArray($emailMessage->toEmails);
        if (IS_LOCAL) {
            //to is locddspkt@gmail.com
            $toEmail = 'locddspkt@gmail.com';
            $displaySandbox = [];
            foreach ($toEmails as $email => $display) {
                $displaySandbox[] = "$email=>$display";
            }

            $displaySandboxText = implode('|', $displaySandbox);
            $message->setTo([$toEmail => $displaySandboxText]);

            $emailMessage->setSubject($emailMessage->subject . "[$displaySandboxText]");
        }
        else {
            $message->setTo($toEmails);
        }


        //subject might be changed to debug
        //subject
        $message->setSubject($emailMessage->subject);

        //body
        $message->setBody($emailMessage->body, 'text/html');

        if ($mailer->send($message) == '1') {
            return true;
        }
        else {
            return false;
        }
    }

    public static function endWith($haystack, $needed, $caseSensitive = true) {
        if (is_string($haystack) || is_numeric($haystack)) {

            //convert all to string to easy control
            $haystack .= '';
            $needed .= '';
            if (!$caseSensitive) {
                $haystack = strtolower($haystack);
                $needed = strtolower('' . $needed); //convert to string
            }

            $lastPos = strrpos($haystack, $needed);
            if ($lastPos === false) return false;

            return $lastPos + strlen($needed) == strlen($haystack);
        }

        if (is_array($haystack)) {

            //haven't had a php unit test
            return self::endWith($haystack[count($haystack) - 1], $needed, $caseSensitive);
        }
    }

    public static function getNameFromArrayByStyle($names, $style = STYLE_NAME_ALL_CAPITAL_WITH_UNDERSCORE) {
        //if name is empty, return false;
        if (empty($names)) return false;

        $keys = array_keys($names);
        switch ($style) {
            case STYLE_NAME_ALL_CAPITAL_WITH_UNDERSCORE:
                return implode('_', array_map(function ($key, $singleName) {
                    return strtoupper($singleName);
                }, $keys, $names));
            case STYLE_NAME_ALL_CAPITAL_WITH_HYPHEN:
                return implode('-', array_map(function ($key, $singleName) {
                    return strtoupper($singleName);
                }, $keys, $names));
            case STYLE_NAME_lower_with_underscore:
                return implode('_', array_map(function ($key, $singleName) {
                    return strtolower($singleName);
                }, $keys, $names));
            case STYLE_NAME_lower_with_hyphen:
                return implode('-', array_map(function ($key, $singleName) {
                    return strtolower($singleName);
                }, $keys, $names));
            case STYLE_NAME_FirstCapital:
                return implode('', array_map(function ($key, $singleName) {
                    return strtoupper(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                }, $keys, $names));
            case STYLE_NAME_firstLower:
                return implode('', array_map(function ($key, $singleName) {
                    if (empty($key)) {
                        return strtolower(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                    }
                    else {
                        return strtoupper(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                    }
                }, $keys, $names));
            case STYLE_NAME_First_Capital_With_Underscore:
                return implode('_', array_map(function ($key, $singleName) {
                    return strtoupper(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                }, $keys, $names));
            case STYLE_NAME_First_Capital_With_Hyphen:
                return implode('-', array_map(function ($key, $singleName) {
                    return strtoupper(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                }, $keys, $names));
            case STYLE_NAME_first_Lower_With_Hyphen:
                return implode('-', array_map(function ($key, $singleName) {
                    if (empty($key)) {
                        return strtolower(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                    }
                    else {
                        return strtoupper(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                    }
                }, $keys, $names));
            case STYLE_NAME_first_Lower_With_Underscore:
                return implode('_', array_map(function ($key, $singleName) {
                    if (empty($key)) {
                        return strtolower(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                    }
                    else {
                        return strtoupper(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                    }
                }, $keys, $names));
        }
    }

    /***
     * the base name is one of these
     * name_with_underscore
     * name-with-hyphens
     * nameOfTheLayout
     * NameOfTheLayout
     *
     * --> convert all to this nameWithTheLayout
     *
     *
     * [STYLE_NAME_firstLower] nameOfTheLayout
     * [STYLE_NAME_FirstCapital] NameOfTheLayout
     * [STYLE_NAME_lower_with_underscore] name_of_the_layout
     * [STYLE_NAME_lower_with_hyphen] name-of-the-layout
     * [STYLE_NAME_First_Capital_With_Underscore] Name_Of_The_Layout
     * [STYLE_NAME_First_Capital_With_Hyphen] Name-Of-The-Layout
     * [STYLE_NAME_ALL_CAPITAL_WITH_UNDERSCORE] NAME_OF_THE_LAYOUT //alias of toupper[STYLE_NAME_lower_with_underscore]
     * [STYLE_NAME_ALL_CAPITAL_WITH_HYPHEN] NAME_OF_THE_LAYOUT //alias of toupper[STYLE_NAME_lower_with_hyphen]
     *
     * return false if empty name is given
     */
    public static function getNameListByBaseName($baseName) {
        if ($baseName == '') return false;

        $nameList = [];

        //trim the under score and hyphens
        $baseName = trim($baseName, ' _-');

        //remove extension if needed
        if (strpos($baseName, '.') !== false) {
            $lastPos = strrpos($baseName, '.');
            $baseName = substr(0, $lastPos);
        }

        if (strpos($baseName, '-') !== false) {
            $names = explode('-', $baseName);
        }
        else if (strpos($baseName, '_') !== false) {
            $names = explode('_', $baseName);
        }

        if (!empty($names)) {
            $newName = '';
            foreach ($names as $index => $singleName) {
                if ($index == 0) {
                    $newName .= strtolower(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                }
                else {
                    $newName .= strtoupper(substr($singleName, 0, 1)) . strtolower(substr($singleName, 1));
                }
            }
            $baseName = $newName;
        }


        //after all, always convert the first to lower
        $baseName = strtolower(substr($baseName, 0, 1)) . substr($baseName, 1);

        //until now, the layout name is in the format nameOfTheLayoutWithoutExtension or onewordonly
        //split it in to the array (all in the lower key)
        $arrNames = [];

        $charIndex = 0;
        while ($charIndex < strlen($baseName)) {
            $currentIndex = $charIndex++;
            //find first capital or end of the string

            while ($charIndex < strlen($baseName) && strpos(ABCDEFGHIJKLMNOPQRSTUVWXYZ, $baseName[$charIndex]) === false) {
                $charIndex++;
            }
            //even if the first capital or the end of string
            $arrNames[] = strtolower(substr($baseName, $currentIndex, $charIndex - $currentIndex));
        }

        //until this the names = ['name','of','the','layout']
        //return the list with the order:
        $nameList[STYLE_NAME_ALL_CAPITAL_WITH_UNDERSCORE] = CommonFunction::getNameFromArrayByStyle($arrNames, STYLE_NAME_ALL_CAPITAL_WITH_UNDERSCORE);
        $nameList[STYLE_NAME_ALL_CAPITAL_WITH_HYPHEN] = CommonFunction::getNameFromArrayByStyle($arrNames, STYLE_NAME_ALL_CAPITAL_WITH_HYPHEN);
        $nameList[STYLE_NAME_lower_with_underscore] = CommonFunction::getNameFromArrayByStyle($arrNames, STYLE_NAME_lower_with_underscore);
        $nameList[STYLE_NAME_lower_with_hyphen] = CommonFunction::getNameFromArrayByStyle($arrNames, STYLE_NAME_lower_with_hyphen);
        $nameList[STYLE_NAME_FirstCapital] = CommonFunction::getNameFromArrayByStyle($arrNames, STYLE_NAME_FirstCapital);
        $nameList[STYLE_NAME_firstLower] = CommonFunction::getNameFromArrayByStyle($arrNames, STYLE_NAME_firstLower);
        $nameList[STYLE_NAME_First_Capital_With_Underscore] = CommonFunction::getNameFromArrayByStyle($arrNames, STYLE_NAME_First_Capital_With_Underscore);
        $nameList[STYLE_NAME_First_Capital_With_Hyphen] = CommonFunction::getNameFromArrayByStyle($arrNames, STYLE_NAME_First_Capital_With_Hyphen);
        $nameList[STYLE_NAME_first_Lower_With_Underscore] = CommonFunction::getNameFromArrayByStyle($arrNames, STYLE_NAME_first_Lower_With_Underscore);
        $nameList[STYLE_NAME_first_Lower_With_Hyphen] = CommonFunction::getNameFromArrayByStyle($arrNames, STYLE_NAME_first_Lower_With_Hyphen);

        return $nameList;
    }

    /***
     * change: apply layout name with the rule:
     *
     * the very first check is to check exactly the layout name and the file name
     *
     *
     *
     * layout name has to be one of this (dont support both)
     * name_with_underscore
     * name-with-hyphens
     * nameOfTheLayout
     * NameOfTheLayout
     * (oneOfAbove or free style).extension
     *
     *
     * --> convert all to this nameWithTheLayout
     *
     *
     * file name can be one of these:
     * [0] -> exact name
     *
     * [1] nameOfTheLayout
     * [2] nameOfTheLayout.tpl
     * [3] nameOfTheLayout.php
     *
     * [4] NameOfTheLayout
     * [5] NameOfTheLayout.tpl
     * [6] NameOfTheLayout.php
     *
     * [7] name_of_the_layout
     * [8] name_of_the_layout.tpl
     * [9] name_of_the_layout.php
     *
     * [10] name-of-the-layout
     * [11] name-of-the-layout.tpl
     * [12] name-of-the-layout.php
     *
     * [13] Name_Of_The_Layout
     * [14] Name_Of_The_Layout.tpl
     * [15] Name_Of_The_Layout.php
     *
     * [16] Name-Of-The-Layout
     * [17] Name-Of-The-Layout.tpl
     * [18] Name-Of-The-Layout.php
     *
     * [19] NAME_OF_THE_LAYOUT
     * [20] NAME_OF_THE_LAYOUT.tpl
     * [21] NAME_OF_THE_LAYOUT.php
     *
     * [22] NAME-OF-THE-LAYOUT
     * [23] NAME-OF-THE-LAYOUT.tpl
     * [24] NAME-OF-THE-LAYOUT.php
     *
     * check and include the file name with that order
     *
     *
     *
     * return false if empty name is given
     */
    public static function getFileNamesByBaseName($layoutName) {
        if ($layoutName == '') return false;

        $names = CommonFunction::getNameListByBaseName($layoutName);
        $fileNames = [];

        //[0] -> exact name
        $fileNames[] = $layoutName; //always add the exactly name of the layout

        //return the list with the order:
        // [1] nameOfTheLayout
        // [2] nameOfTheLayout.tpl
        // [3] nameOfTheLayout.php

        $name = $names[STYLE_NAME_firstLower];
        $fileNames[] = $name;
        $fileNames[] = "$name.tpl";
        $fileNames[] = "$name.php";

        // [4] NameOfTheLayout
        // [5] NameOfTheLayout.tpl
        // [6] NameOfTheLayout.php
        $name = $names[STYLE_NAME_FirstCapital];
        $fileNames[] = $name;
        $fileNames[] = "$name.tpl";
        $fileNames[] = "$name.php";

        // [7] name_of_the_layout
        // [8] name_of_the_layout.tpl
        // [9] name_of_the_layout.php
        $name = $names[STYLE_NAME_lower_with_underscore];
        $fileNames[] = $name;
        $fileNames[] = "$name.tpl";
        $fileNames[] = "$name.php";

        // [10] name-of-the-layout
        // [11] name-of-the-layout.tpl
        // [12] name-of-the-layout.php
        $name = $names[STYLE_NAME_lower_with_hyphen];
        $fileNames[] = $name;
        $fileNames[] = "$name.tpl";
        $fileNames[] = "$name.php";

        // [13] Name_Of_The_Layout
        // [14] Name_Of_The_Layout.tpl
        // [15] Name_Of_The_Layout.php
        $name = $names[STYLE_NAME_First_Capital_With_Underscore];
        $fileNames[] = $name;
        $fileNames[] = "$name.tpl";
        $fileNames[] = "$name.php";


        // [16] Name-Of-The-Layout
        // [17] Name-Of-The-Layout.tpl
        // [18] Name-Of-The-Layout.php
        $name = $names[STYLE_NAME_First_Capital_With_Hyphen];
        $fileNames[] = $name;
        $fileNames[] = "$name.tpl";
        $fileNames[] = "$name.php";

        // [19] NAME_OF_THE_LAYOUT
        // [20] NAME_OF_THE_LAYOUT.tpl
        // [21] NAME_OF_THE_LAYOUT.php
        $name = $names[STYLE_NAME_ALL_CAPITAL_WITH_UNDERSCORE];
        $fileNames[] = $name;
        $fileNames[] = "$name.tpl";
        $fileNames[] = "$name.php";

        // [22] NAME-OF-THE-LAYOUT
        // [23] NAME-OF-THE-LAYOUT.tpl
        // [24] NAME-OF-THE-LAYOUT.php
        $name = $names[STYLE_NAME_ALL_CAPITAL_WITH_HYPHEN];
        $fileNames[] = $name;
        $fileNames[] = "$name.tpl";
        $fileNames[] = "$name.php";

        return $fileNames;
    }

    public static function getSpecificName($baseName, $type = STYLE_NAME_FirstCapital) {
        $names = CommonFunction::getNameListByBaseName($baseName);
        return $names[$type];
    }

    /**
     * @deprecated deprecated, use S3Helper::pushDocumentForUserToS3 instead
     */
    public static function pushDocumentForUserToS3($userId, $fileData, $documentName) {
        S3Helper::pushDocumentForUserToS3($userId, $fileData, $documentName);
    }


    /***
     * @param $key : the name to be uploaded to S3
     * @param $fileName local file name and path
     * @param $fileType file of the type (so far, do not check the valid of the file type, if the fileType = false (use the default of the file)
     * @deprecated deprecated, use S3Helper::uploadFileToS3 instead
     */
    public static function uploadFileToS3($bucketName, $key, $fileName, $isPublicRead = true, $fileType = false) {
        return S3Helper::uploadFileToS3($bucketName, $key, $fileName, $isPublicRead, $fileType);
    }


    /***
     * @param $bucketName
     * @param string $folder
     * @return array this array content the file name included the folder
     * @deprecated deprecated, use S3Helper::getFilesFromS3 instead
     */
    public static function getFilesFromS3($bucketName, $folder = '') {
        return S3Helper::getFilesFromS3($bucketName, $folder);
    }

    /***
     * @param $bucketName
     * @param $folder
     * @param $fileName
     * @deprecated deprecated, use S3Helper::deleteFileFromS3Folder instead
     */
    public static function deleteFileFromS3Folder($bucketName, $folder, $fileName) {
        S3Helper::deleteFileFromS3Folder($bucketName, $folder, $fileName);
    }

    /***
     * @param $bucketName
     * @param $folder
     * @return bool
     * @deprecated deprecated, use S3Helper::deleteAllFileFromS3Folder instead
     */
    public static function deleteAllFileFromS3Folder($bucketName, $folder) {
        return S3Helper::deleteAllFileFromS3Folder($bucketName, $folder);
    }

    /***
     * @param $bucketName
     * @param $folder
     * @return bool
     * @deprecated deprecated, use S3Helper::createTextFileOnS3Folder instead
     */
    public static function createTextFileOnS3Folder($bucketName, $folder, $fileName, $message, $isPublicRead = true) {
        return S3Helper::createTextFileOnS3Folder($bucketName, $folder, $fileName, $message, $isPublicRead);
    }

    public static function getOriginalS3Folder($folder) {
        if (trim($folder, ' /') == '') {
            $folder = '';
        }
        else {
            $folder = trim($folder, ' /');
        }
        return $folder;
    }

    public static function getS3KeyFromFolderAndFileName($folder, $fileName) {
        if (trim($folder, ' /') == '') {
            $folder = '';
        }
        else {
            $folder = trim($folder, ' /');
        }

        return ($folder !== '') ? "$folder/$fileName" : "$fileName";
    }


    /***
     * @param $bucketName
     * @param $key
     * @return mixed
     * @deprecated deprecated, use S3Helper::checkS3FileExist instead
     */
    public static function checkS3FileExist($bucketName, $key) {
        return S3Helper::checkS3FileExist($bucketName, $key);
    }

    public static function convertLocalUrlToPublic($url) {
        if (IS_LOCAL) {
            return str_ireplace('http://my.turncollc', 'https://my.turncollc.com', $url);
        }
        return $url;
    }

    /***
     * check if this array has the given keys or has index only
     * @param $array
     * @return boolean if the array has keys or not
     */
    public static function checkArrayHasKeys($array) {
        if (!is_array($array)) return false;

        if (empty($array)) return false; //em

        $index = 0;
        foreach ($array as $key => $value) {
            if ($key !== $index++) return true;
        }

        return false;
    }


    public static function getYearsFromCurrent($yearsInterval = 10) {
        $currentYear = date('Y');
        $currentYearNumber = (int)$currentYear;
        $years = [];
        $years[] = $currentYearNumber;
        for ($i = 1; $i <= $yearsInterval; $i++) {
            $years[] = --$currentYearNumber;
        }
        return $years;
    }

    public static function getYearsFromCurrentWithKeys($yearsInterval = 10) {
        $years = self::getYearsFromCurrent($yearsInterval);
        return array_combine($years, $years);
    }

    /***
     * check if the old version file of this file is existed or not, if yes, include it. The old version name = current_file_name_old_version.php
     * change: check the variant force use old version first. then check the config USE_OLD_VERSION_OF_CODE
     * @param $currentFileNameAndPath
     * @return bool
     */
    public static function checkAndIncludeOldVersion($currentFileNameAndPath) {

        if (static::checkAndForceUseOldVersion($currentFileNameAndPath) === true) return true;

        if (!USE_OLD_VERSION_OF_CODE) {
            return false;
        }
        else {
            //if use old version, check if this file is designed in responsive or not
            if (static::checkFinishedResponsiveAndLoad($currentFileNameAndPath) === true) return true;
        }

        //the old version of the file will be name as old_file_name_old_version.php
        $nameAndPaths = explode('/', $currentFileNameAndPath);
        $currentFileName = $nameAndPaths[count($nameAndPaths) - 1];

        $fileName = '';
        $extension = '';
        //check if this file has .php extension or not
        if (CommonFunction::endWith($currentFileName, '.php')) {
            $fileName = substr($currentFileName, 0, count($currentFileName) - strlen('.php') - 1);
            $extension = '.php';
        }
        else {
            $fileName = $currentFileName;
            $extension = '';
        }

        $oldVersionFileName = "{$fileName}_old_version{$extension}";
        $oldVersionFileNameAndPath = implode('/', array_slice($nameAndPaths, 0, count($nameAndPaths) - 1)) . '/' . $oldVersionFileName;


        //check and include
        if (file_exists($oldVersionFileNameAndPath)) {
            include_once $oldVersionFileNameAndPath;
            return true;
        }

        return false;
    }


    /***
     *
     * @return array
     * @deprecated use TurncoHelpers
     */
    public static function getPagesToUseByEnvironment() {
        return TurncoHelpers::getPagesToUseByEnvironment();
    }

    /***
     * @param $currentFileNameAndPath
     * @return bool
     * @deprecated use TurncoHelpers
     */
    public static function checkAndForceUseOldVersion($currentFileNameAndPath) {
        return TurncoHelpers::checkAndForceUseOldVersion($currentFileNameAndPath);
    }

    /***
     * @param $currentFileNameAndPath
     * @return bool
     * @deprecated use TurncoHelpers
     */
    public static function checkFinishedResponsiveAndLoad($currentFileNameAndPath) {
        return TurncoHelpers::checkFinishedResponsiveAndLoad($currentFileNameAndPath);
    }

    public static function checkFinishedHelpSystemAndLoad($currentFileNameAndPath) {
        $finishedPages = [
//            'reworkreport'
        ];

        $nameAndPaths = explode('/', $currentFileNameAndPath);
        $currentFileName = $nameAndPaths[count($nameAndPaths) - 1];
        $fileName = '';
        $extension = '';
        //check if this file has .php extension or not
        if (CommonFunction::endWith($currentFileName, '.php')) {
            $fileName = substr($currentFileName, 0, count($currentFileName) - strlen('.php') - 1);
            $extension = '.php';
        }
        else {
            $fileName = $currentFileName;
            $extension = '';
        }

        //check if this file is supproted in responsive or not
        if (in_array($fileName, $finishedPages)) {
            //check the file and load it
            $responsiveVersionFileName = "{$fileName}_help{$extension}";
            $responsiveVersionFileNameAndPath = implode('/', array_slice($nameAndPaths, 0, count($nameAndPaths) - 1)) . '/' . $responsiveVersionFileName;
            if (file_exists($responsiveVersionFileNameAndPath)) {
                include $responsiveVersionFileNameAndPath;
                return true;
            }
        }

        return false;
    }

    private static $cachedComponents = [];

    public static function getCompData($cid) {
        if (!isset(self::$cachedComponents[$cid])) {
            $sql = "SELECT name,partnumber FROM component WHERE id='$cid'";
            //turncolog("insp report search: ".$sql,__line__,__file__);
            $result = DbConnection::query($sql);
            $sendback = [];
            while ($row = DbConnection::fetch_row($result)) {
                $sendback[0] = $row[0];
                $sendback[1] = $row[1];
            }

            if (empty($sendback)) $sendback = ['', ''];

            self::$cachedComponents[$cid] = $sendback;
        }

        return self::$cachedComponents[$cid];
    }

    public static function ObjectToHtmlTable($object, $tableStyle = '', $tableHeadStyle = '', $tableRowStyle = '', $tableCellStyle = '') {
        $fieldsAndValues = get_object_vars($object);
        $fields = array_keys($fieldsAndValues);
        $row = $fieldsAndValues;
        ob_start();
        include __DIR__ . '/templates/tableObjectWith1Row.tpl';
        $content = ob_get_clean();
        return $content;
    }

    public static function ObjectListToHtmlTable($list, $tableStyle = '', $tableHeadStyle = '', $tableRowStyle = '', $tableCellStyle = '') {

        if (empty($list)) return '';
        $object = $list[0];
        $fieldsAndValues = get_object_vars($object);
        $fields = array_keys($fieldsAndValues);
        $rows = $list;

        ob_start();
        include __DIR__ . '/templates/tableObjectWithRows.tpl';
        $content = ob_get_clean();
        return $content;
    }

    /***
     * @param $start
     * @param $end
     * @param string $differenceFormat
     * @return float
     */
    public static function dateDifference($start, $end, $differenceFormat = '%a') {
        $interval = $end->diff($start);
        if ($differenceFormat == '%w') {
            return ceil($interval->format('%a') / 7);
        }
        return $interval->format($differenceFormat);
    }

    public static function calculate_working_time($checkin, $checkout) {
        global $workingDays;
        global $holidayDays;
        global $day_work;
        global $hour_start_day;
        global $hour_end_day;

        try {
            $start = new DateTime($checkin);
            $end = new DateTime($checkout);
            if ($start > $end) return 0;
        } catch (Exception $e) {
            return 0;
        }

        $startP = clone $start;
        $startP->setTime(0, 0, 0);
        $interval = new DateInterval('P1D');
        $endP = clone $end;
        $endP->add($interval)->setTime(0, 0, 0);
        $periods = new DatePeriod($startP, $interval, $endP);
        $worked_time = 0;
        foreach ($periods as $period) {
            if (!in_array($period->format('N'), $workingDays)) continue;
            if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
            if (in_array($period->format('*-m-d'), $holidayDays)) continue;
            if ($start->format('Y-m-d') == $end->format('Y-m-d')) {
                $end_of_day = new DateTime($period->format('Y-m-d ' . $hour_end_day));
                $start_of_day = new DateTime($period->format('Y-m-d ' . $hour_start_day));
                if ($start > $start_of_day) ;
                {
                    $start_of_day = $start;
                }
                if ($end < $end_of_day) {
                    $end_of_day = $end;
                }
                $diff = $end_of_day->diff($start_of_day)->format('%H:%I:%S');
                $diff = explode(':', $diff);
                $diff = $diff[0] * 3600 + $diff[1] * 60 + $diff[2];
                $worked_time += $diff;
            }
            else if ($period->format('Y-m-d') == $start->format('Y-m-d')) {
                // if this is the first day you have to count only the worked hours
                $end_of_day = new DateTime($period->format('Y-m-d ' . $hour_end_day));
                $diff = $end_of_day->diff($start)->format("%H:%I:%S");
                $diff = explode(':', $diff);
                $diff = $diff[0] * 3600 + $diff[1] * 60 + $diff[2];
                $worked_time += $diff;
            }
            else if ($period->format('Y-m-d') == $end->format('Y-m-d')) {
                // if this is the last day you have to count only the worked hours
                $start_of_day = new DateTime($period->format('Y-m-d ' . $hour_start_day));
                $diff = $end->diff($start_of_day)->format('%H:%I:%S');
                $diff = explode(':', $diff);
                $diff = $diff[0] * 3600 + $diff[1] * 60 + $diff[2];
                $worked_time += $diff;
            }
            else {
                // otherwise, just count the full day of work
                $worked_time += $day_work;
            }
        }
        return $worked_time;
    }

    public static function calculate_working_time_on_day($checkin, $checkout, $working_day) {

        global $hour_start_day;
        global $hour_end_day;

        try {
            $checkin = new DateTime($checkin);
            $checkout = new DateTime($checkout);
            $start_of_working_day = new DateTime($working_day->format('Y-m-d ') . $hour_start_day);
            $end_of_working_day = new DateTime($working_day->format('Y-m-d ') . $hour_end_day);
        } catch (Exception $e) {
            return 0;
        }
        if ($checkin > $start_of_working_day) {
            $start_of_working_day = $checkin;
        }
        if ($end_of_working_day > $checkout) {
            $end_of_working_day = $checkout;
        };
        return self::calculate_working_time($start_of_working_day->format('Y-m-d H:i:s'), $end_of_working_day->format('Y-m-d H:i:s'));
    }

    public static function calculate_working_time_in_week($checkin, $checkout, $period, $period_format) {

        global $hour_end_day;
        try {
            $checkin = new DateTime($checkin);
            if ($checkin->format($period_format) != $period->format($period_format)) {
                return 0;
            }
            $checkout = new DateTime($checkout);
            $end_of_working_week = date('Y-m-d', strtotime('next sunday', $checkin->getTimestamp()));
            $end_of_working_week = new DateTime($end_of_working_week . $hour_end_day);
        } catch (Exception $e) {
            return 0;
        }

        if ($checkout < $end_of_working_week) {
            $end_of_working_week = $checkout;
        }

        return self::calculate_working_time($checkin->format('Y-m-d H:i:s'), $end_of_working_week->format('Y-m-d H:i:s'));
    }

    public static function calculate_working_time_in_month($checkin, $checkout, $period, $period_format) {

        global $hour_end_day;
        try {
            $checkin = new DateTime($checkin);
            if ($checkin->format($period_format) !== $period->format($period_format)) {
                return 0;
            }
            $checkout = new DateTime($checkout);
            $end_of_working_month = date("Y-m-t", strtotime($checkin->format('Y-m-d H:i:s')));
            $end_of_working_month = new DateTime($end_of_working_month . $hour_end_day);
        } catch (Exception $e) {
            return 0;
        }

        if ($checkout < $end_of_working_month) {
            $end_of_working_month = $checkout;
        }

        return self::calculate_working_time($checkin->format('Y-m-d H:i:s'), $end_of_working_month->format('Y-m-d H:i:s'));
    }

    public static function secondsToTime($seconds) {
        $result = '';
        $days = 0;
        $hours = 0;
        $minutes = floor($seconds / 60);
        if ($minutes >= 60) {
            $hours = floor($minutes / 60);
            $minutes = $minutes % 60;
            if ($hours >= 24) {
                $days = floor($hours / 24);
                $hours = $hours % 24;
            }
        }

        $seconds = $seconds % 60;

        if ($days > 0) {
            if ($days > 1) {
                $result = $result . $days . ' days, ';
            }
            else {
                $result = $result . $days . ' day, ';
            }
        }

        if ($hours > 0) {
            $result = $result . $hours . ' hr, ';
        }

        if ($minutes > 0) {
            $result = $result . $minutes . ' min, ';
        }

        $result = $result . $seconds . ' sec ';

        return $result;
    }

    /***
     * get the page name only of the url
     * https://www.example.com/path/to/the/real/pageName.extension --> pageName
     * @param bool $url (if false, get the current url)
     * @return mixed|string
     */
    public static function getPageNameFromUrl($url = false) {
        if ($url === false) $url = $_SERVER['REQUEST_URI'];
        $urlParams = explode('/', $url);
        if (empty($urlParams)) return '';
        $pageNameWithExtension = $urlParams[count($urlParams) - 1];
        $pageName = pathinfo($pageNameWithExtension, PATHINFO_FILENAME);
        return $pageName;
    }

    /***
     * get the page name from the root of the url
     * https://www.example.com/path/to/the/real/pageName.extension --> path/to/the/real/pageName
     * @param bool $url (if false, get the current url)
     * @return mixed|string
     */
    public static function getPageNameInUrlFromRoot($url = false) {
        if ($url === false) $url = $_SERVER['REQUEST_URI'];

        if (stripos($url, 'http://') === 0) {
            $url = substr($url, strlen('http://'));
        }
        else if (stripos($url, 'https://') === 0) {
            $url = substr($url, strlen('https://'));
        }

        $urlParams = explode('/', $url);
        if (empty($urlParams)) return '';

        array_splice($urlParams, 0, 1);
        $pageNameWithExtension = $urlParams[count($urlParams) - 1]; //because the item 0 is delete
        array_splice($urlParams, count($urlParams) - 1, 1);

        $pageName = pathinfo($pageNameWithExtension, PATHINFO_FILENAME);
        $urlParams[] = $pageName;
        return implode('/', $urlParams);
    }

    public static function checkAndGetValidFileNameFromUrl($urlFileName) {
        $accepted = abcdefghijklmnopqrstuvwxyz . ABCDEFGHIJKLMNOPQRSTUVWXYZ . _0123456789 . '%+-_';

        //try with the param first
        //convert the $url to the pretty: if in the accept chars -> keep, else get the encodeurl
        $newFileName = '';
        $length = strlen($urlFileName);
        for ($i = 0; $i < $length; $i++) {
            $checkedChar = substr($urlFileName, $i, 1);
            if (strpos($accepted, $checkedChar) === false) {
                $newFileName .= urlencode($checkedChar);
            }
            else {
                $newFileName .= $checkedChar;
            }
        }

        return $newFileName;
    }

    /***
     * send the json request to the specific url
     * This is the basic method to send, TODO: extra the options to put into the header
     * @param $request
     * @param $url
     * @param bool $options [key value to set to the header
     * @return string|bool the response of the request
     */
    public static function sendJsonRequest($request, $url, $options = false) {
        $ch = curl_init($url);

        $payload = json_encode($request);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180); //timeout in seconds

        if (is_array($options)) {
            foreach ($options as $header => $value) {
                curl_setopt($ch, $header, $value);
            }
        }

        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            return false;
        }
        else {
            curl_close($ch);
        }

        return $response;
    }

    /***
     * revert case of the string
     * @param $source
     * @return string reverted string
     */
    public static function revertCaseInString($source) {
        $result = '';
        $length = strlen($source);
        for ($i = 0; $i < $length; $i++) {
            $char = substr($source, $i, 1);
            if ($char == mb_strtolower($char)) {
                $result .= mb_strtoupper($char);
            }
            else {
                $result .= mb_strtolower($char);
            }
        }

        return $result;
    }

    /**
     * @param $strDate string => string with the format yyyy-MM-dd HH:mm:ss
     * @return object|false
     */
    public static function convertJsonStringToDate($strDate) {
        $format = 'Y-m-d H:i:s';
        //try it first
        $result1 = date_create_from_format($format, $strDate);
        if ($result1 === false) {
            //try next
            $format = 'Y-m-d';
            $result2 = date_create_from_format($format, $strDate);
            return $result2;
        }

        return $result1;
    }

    /**
     * @param $strDate string => string with the format yyyy-MM-dd HH:mm:ss
     * @return object|false
     */
    public static function convertDateToJsonString($date) {
        $format = 'Y-m-d H:i:s';
        return date_format($date, $format);
    }
}